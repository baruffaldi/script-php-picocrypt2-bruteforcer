<?php
/**
 * PicoCrypt2 bruteforcer ( ASCII files )
 *
 * @author bornslippy < mdb [at] insaneminds [dot] org >
 * @uses mcrypt ( http://mcrypt.sourceforge.net/ )
 * @todo BINARY files decrypter
 * @package PicoCrypt2 bruteforcer
 */
/*
Details:
    Blowfish encryption algorithm
	CBC mode
	Key: 128-bits key 
	Key-Password: MD5 message-digest of user password
*/

if ( ! empty( $argv ) )
foreach ( $argv as $value )
{
    $value_array = @explode( '=', $value );
    $argv[$value_array[0]] = $value_array[1];
}

// debug mode switcher
ini_set( 'display_errors', ( array_key_exists( '--debug', $argv ) ) ? 1 : 0 );

// print header
print "[*] File decrypt bruteforcer\n"
    . "[*] Author: bornslippy (mdb@insaneminds.org)\n"
    . "[*] \n"
    . "[*] Algorithm: blowfish\n"
    . "[*] Mode: CBC\n"
    . "[*] Key Lenght: 128-bits\n"
    . "[*] \n"
    . "[*] ";

// check requirements
if ( empty( $argv['--dictionary'] ) || empty( $argv['--file'] ) )
    exit( "You have to specify the dictionary to use and the target file!\n" 
        . "[*] Example: php {$_SERVER['argv'][0]} --dictionary=dictionary.txt --file=target.txt\n" );
        
elseif ( ! file_exists( $argv['--dictionary'] ) )
    exit( "You have to specify a correct dictionary!\n"
        . "[*] The specified dictionary: {$argv['--dictionary']} doesn't exists!\n" );
        
elseif ( ! file_exists( $argv['--file'] ) )
    exit( "You have to specify a correct file target!\n"
        . "[*] The specified target file: {$argv['--file']} doesn't exists!\n" );

// prepare all data
$passwords = explode( "\n", file_get_contents( $argv['--dictionary'] ) );
$end       = count( $passwords );
$esito     = FALSE;

print "Let's bruteforce $end passwords!\n[";

// encryption settings
$mcbf_key_len = 128;
$mcbf_mode    = ( empty( $argv['--mode'] ) ) ? 'cbc' : $argv['--mode'];
$mcbf_data    = file_get_contents( $argv['--file'] );
$mcbf_cipher  = mcrypt_module_open( MCRYPT_BLOWFISH , NULL, mcbf_mode, NULL );

// bruteforce algorithm cycle
foreach ( $passwords as $password )
{
    if ( ! $esito )
    {
        mcrypt_generic_init( $mcbf_cipher, md5( $password ), $iv );
        $decrypted_data = mcrypt_generic( $mcbf_cipher, $mcbf_data );
        mcrypt_generic_deinit( $mcbf_cipher );
        
        if ( $decrypted_data != FALSE ) $rightPassword = $password;
        print ".";
    }
}
proc_open();

"
Usage: mcrypt [-dFusgbhLvrzp] [-f keyfile] [-k key1 key2 ...] [-m mode] [-o keymode] [-s keysize] [-a algorithm] [-c config_file] [file ...]

     -g, --openpgp            Use the OpenPGP (RFC2440) file format.
     --openpgp-z INTEGER      Sets the compression level for openpgp
                              packets (0 disables).
     -d, --decrypt            decrypts.
     -s, --keysize INTEGER    Set the algorithm's key size (in
                              bytes).
     -o, --keymode KEYMODE    Specify the keyword mode. Use the
                              --list-keymodes parameter to view all
                              modes.
     -f, --keyfile FILE       Specify the file to read the keyword
                              from.
     -c, --config FILE        Use configuration file FILE.
     -a, --algorithm ALGORITHM
                              Specify the encryption and decryption
                              algorithm. Use the --list parameter to
                              see the supported algorithms.
     --algorithms-directory DIRECTORY
                              Set the algorithms directory.
     -m, --mode MODE          Specify the encryption and decryption
                              mode. Use the --list parameter to see
                              the supported modes.
     --modes-directory DIRECTORY
                              Set the modes directory.
     -h, --hash HASH          Specify the hash algorithm to be used.
                              Use the --list-hash parameter to view
                              the hash algorithms.
     -k, --key KEY1 KEY2...KEYN
                              Specify the key(s)
     --noiv                   Do not use an IV.
     -b, --bare               Do not keep algorithm information in
                              the encrypted file.
     -z, --gzip               Use gzip to compress files before
                              encryption.
     -p, --bzip2              Use bzip2 to compress files before
                              encryption.
     --flush                  Immediately flush the output
     -l, --doublecheck        Double check passwords.
     -u, --unlink             Unlink the input file after encryption
                              or decryption.
     --nodelete               Do not delete the output file if
                              decryption failed.
     -t, --time               Prints timing information.
     -F, --force              Forces output to stdout.
     --echo                   Echo asterisks when entering the
                              password.
     -r, --random             Use real random data (if your system
                              supports it).
     --list                   Prints a list of the supported
                              algorithms and modes.
     --list-keymodes          Prints a list of the supported key
                              modes.
     --list-hash              Prints a list of the supported hash
                              algorithms.
     -V, --verbose            More information is displayed.
     -q, --quiet              Suppress some non critical warnings.
     --help                   Prints this help
     -v, --version            Prints the version number
     -L, --license            Displays license information.
"
print "]\n[*] done.\n[*]\n";

// results
print ( $esito ) ? "[*] Password found!\n"
                 . "[*] The password you are searching for is: $rightPassword\n"
                 . "[*] Enjoy the bornslippy's life!\n"
                 . "[*]\n" 
                 : "[*] Right password not found!\n"
                 . "[*] Try to change your dictionary :)\n"
                 . "[*]\n" ;
