<?php
/*
File de-crypt bruteforcer
Author: bornslippy (mdb@insaneminds.org)

	Blowfish encryption algorithm
	CBC mode
	Key: 128-bits key 
	Key-Password: MD5 message-digest of user password
*/

print "[*] File decrypt bruteforcer\n"
    . "[*] ";

    if ( empty( $_SERVER['argv'][1] ) )         exit( "You have to specify the dictionary to use!\n Example: php {$_SERVER['argv'][0]} dictionary.txt" );
elseif ( ! file_exists( $_SERVER['argv'][1] ) ) exit( "You have to specify a correct dictionary\n The specified dictionary: {$_SERVER['argv'][1]} doesn't exists!\n" );

$passwords = file_get_contents( $_SERVER['argv'][1] );
$end       = count( $passwords );

print "Let's bruteforce $end passwords!\n[";

foreach ( $passwords as $password )
{
    if ( ! $esito )
	{
	    $bf_key = md5( $password );
	    $bf_key_len = 128;
	    $bf_mode = 'CBC';
	
	    // Blowfish encryption
	    $esito = blowfish_decrypt( 'p.xls', $bf_mode, $bf_key, $bf_key_len );
	    if ( $esito || array_search( $password, $passwords ) == $end ) 
	    {
	        $rightPassword = $password;
            print "]\n[*] done.\n[*]\n";
	    }
	    else print ".";
	}
}

print ( $esito ) ? "[*] Password found!\n[*] The password you are searching for is: $rightPassword\n[*] Enjoy the bornslippy's life!\n[*]\n" : "[*] Right password not found!\n[*] Try to change your dictionary :)\n[*]\n" ;